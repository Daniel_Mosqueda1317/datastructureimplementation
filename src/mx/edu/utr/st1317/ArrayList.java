/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.st1317;

import mx.edu.utr.datastructures.*;

/**
 *
 * @author Daniel Mosqueda
 */
public class ArrayList implements List {

    public Object[] elements;
    private int size;

    public ArrayList(int initialCapacity) {
        elements = new Object[initialCapacity];
    }

    public ArrayList() {
        this(10);
    }

    /**
     * /the add method is used to add a value to the array in position 0 / / /
     */
    @Override
    public boolean add(Object element) {
        ensureCapacity(size + 1);
        elements[size++] = element;
        return true;
    }

    /**
     * /the add in index method is used to add a value to the array in position
     * that the user wants / / /
     */
    @Override
    public void add(int index, Object element) {
        rangeCheckForAdd(index);
        ensureCapacity(size + 1);
        elements[index] = element;
        size++;

    }

    /**
     * /clear is to delete de value en the array and it position will be null /
     * / /
     */
    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
            size = 0;
        }
    }

    @Override
    public Object get(int index) {
        if (index >= size) {

        }
        return elements[index];
    }

    @Override
    /**
     * Returns the index in this list of the first occurrence of the specified
     * element, or -1 if this list does not contain this element.
     */
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (o.equals(elements[i])) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(elements[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }
    /**
     * Removes the element at the specified position in this list.
     */
    @Override
    public Object remove(int index) {
        outOfBound(index);
        Object oldElement = elements[index];
        int numberMoved = size - index - 1;
        if (numberMoved > 0) {
            System.arraycopy(elements, index + 1, elements, index, numberMoved);
        }
        elements[--size] = null;
        return oldElement;
    }

    @Override
    public Object set(int index, Object element) {
        if (index > size) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        Object old = elements[index];
        elements[index] = elements;
        return old;

    }

    @Override
    public int size() {
        return size;
    }

    private void ensureCapacity(int minCapacity) {

        int oldCapacity = elements.length;
        if (minCapacity > oldCapacity);
        {
            int newcapacity = oldCapacity * 2;

        }

    }

    private void rangeCheckForAdd(int index) {
        if (index < 0 || index > size) {

        }

    }

    private void outOfBound(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException("Out Of Bound");
        }
    }

}
